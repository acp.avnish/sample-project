package com.avnish.sample.metrics.controller;

import com.avnish.sample.metrics.model.payload.MetricsRequest;
import com.avnish.sample.metrics.service.MetricsService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author avnish
 */
@RestController
@RequestMapping("/metrics")
public class MetricsController {
    @Autowired
    private MetricsService metricsService;

    @GetMapping("/all")
    @ApiOperation(value = "get all metrics", response = ResponseEntity.class)
    public ResponseEntity<?> getAllAPIMetrics() {
        return ResponseEntity.ok(metricsService.getAllApiMetrics());
    }

    @GetMapping("/{requestId}")
    public ResponseEntity<?> getAPIMetrics(@PathVariable("requestId") String requestId) {
        return ResponseEntity.ok(metricsService.getMetricsByRequestId(requestId));
    }

    @PostMapping("/publish")
    public ResponseEntity<?> pushAPIMetrics(MetricsRequest metricsRequest) {
        return ResponseEntity.ok(metricsService.createMetrics(metricsRequest));
    }

    @GetMapping("/api")
    public ResponseEntity<?> getMetricsByName(@RequestParam("apiUrl") String apiUrl) {
        return ResponseEntity.ok(metricsService.getAllApiMetrics());
    }

}
