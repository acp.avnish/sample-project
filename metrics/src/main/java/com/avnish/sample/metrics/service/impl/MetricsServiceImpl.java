package com.avnish.sample.metrics.service.impl;

import com.avnish.sample.metrics.model.APIMetrics;
import com.avnish.sample.metrics.model.payload.MetricsRequest;
import com.avnish.sample.metrics.repository.MetricsRepository;
import com.avnish.sample.metrics.service.MetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author avnish
 */
@Service
@Transactional
public class MetricsServiceImpl implements MetricsService {

    @Autowired
    private MetricsRepository metricsRepository;

    @Override
    public List<APIMetrics> getAllApiMetrics() {
        return metricsRepository.findAll();
    }

    @Override
    public APIMetrics getMetricsByRequestId(String id) {
        return metricsRepository.findById(id).orElse(null);
    }

    @Override
    public APIMetrics updateMetrics(String userId, String requestId, MetricsRequest metricsRequest) {
        APIMetrics apiMetrics = new APIMetrics();
        apiMetrics.setMethodName(metricsRequest.getMethodName());
        apiMetrics.setArguments(metricsRequest.getArguments());
        apiMetrics.setRequestTime(metricsRequest.getRequestTime());
        apiMetrics.setResponse(metricsRequest.getResponse());
        apiMetrics.setTimeTaken(metricsRequest.getTimeTaken());
        apiMetrics.setRequestId(requestId);
        return metricsRepository.save(apiMetrics);
    }

    @Override
    public APIMetrics createMetrics(MetricsRequest metricsRequest) {
        APIMetrics apiMetrics = new APIMetrics();
        apiMetrics.setMethodName(metricsRequest.getMethodName());
        apiMetrics.setArguments(metricsRequest.getArguments());
        apiMetrics.setRequestTime(metricsRequest.getRequestTime());
        apiMetrics.setResponse(metricsRequest.getResponse());
        apiMetrics.setTimeTaken(metricsRequest.getTimeTaken());
        return metricsRepository.save(apiMetrics);
    }
}