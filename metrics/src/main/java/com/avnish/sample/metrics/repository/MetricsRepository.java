package com.avnish.sample.metrics.repository;

import com.avnish.sample.metrics.model.APIMetrics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author avnish
 */
@Repository
public interface MetricsRepository extends JpaRepository<APIMetrics, String> {
}