package com.avnish.sample.metrics.service;

import com.avnish.sample.metrics.model.APIMetrics;
import com.avnish.sample.metrics.model.payload.MetricsRequest;

import java.util.List;

/**
 * @author avnish
 */
public interface MetricsService {

    public List<APIMetrics> getAllApiMetrics();

    public APIMetrics getMetricsByRequestId(String id);

    public APIMetrics updateMetrics(String userId, String requestId, MetricsRequest metricsRequest);

    public APIMetrics createMetrics(MetricsRequest metricsRequest);
}