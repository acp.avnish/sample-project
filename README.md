# Sample-Project

* This project is in Spring Boot
* Used Spring security for api security, JPA for db operations, swagger for api documentation, spring actuator for monitoring
spring AOP for logging and capture data, 
* There are 2 modules main module and metrics module.
    * main module: will handle all the main task Rest api
    * metrics module: will handle all the metrics related task with different db. 
* Metrics module will be processing api metrics related task.

* Swagger link for list of api 
http://ec2-52-66-72-135.ap-south-1.compute.amazonaws.com:9080/swagger

* Tested API with the Jmeter [Image](https://gitlab.com/acp.avnish/sample-project/blob/master/Jmeter.png)

* Hosted on AWS ec2

