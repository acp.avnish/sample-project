package com.avnish.sample.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@Configuration
public class SwaggerAPIConfig {

    ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Sample Project").description("Sample Rest API ").termsOfServiceUrl("")
                .version("1.0").contact(
                        new Contact("Avnish Choudhary", "https://gitlab.com/acp.avnish/sample-project",
                                "mail@avnishchoudhary.com")).build();
    }

    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("public-api").apiInfo(apiInfo()).select()
                .apis(RequestHandlerSelectors.basePackage("com.avnish.sample.controller"))
                .paths(PathSelectors.regex("/.*")).build();
    }

    @Bean
    public Docket configureControllerPackageAndConvertors() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("com.avnish.sample.controller")).build().apiInfo(apiInfo());
    }

}