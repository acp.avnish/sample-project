package com.avnish.sample.config;

import com.avnish.sample.security.AuthTokenProvider;
import com.avnish.sample.security.TokenAuthenticationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;

/**
 * @author avnish
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private AuthTokenProvider authTokenProvider;

    /*
        @Override protected void configure(HttpSecurity http) throws Exception {
            //@formatter:off
            http.httpBasic().disable().csrf().disable().sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
                    .antMatchers("/auth/signin").permitAll().antMatchers(HttpMethod.GET, "/vehicles/**").permitAll()
                    .antMatchers(HttpMethod.DELETE, "/vehicles/**").hasRole("ADMIN")
                    .antMatchers(HttpMethod.GET, "/v1/vehicles/**").permitAll().anyRequest().authenticated().and()
                    .apply(new SecurityConfigurer(authTokenProvider));
            //@formatter:on
        }
    */

    @Autowired
    private AuthenticationEntryPoint unauthorizedHandler;

    @Bean
    TokenAuthenticationFilter authenticationFilter() {
        return new TokenAuthenticationFilter(authTokenProvider);
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
       // http.cors().and().csrf().disable().exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
       //         .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().authorizeRequests()
         //       .antMatchers("/", "/favicon.ico", "/**/*.png", "/**/*.gif", "/**/*.svg", "/**/*.jpg", "/**/*.html",
        //                "/**/*.css", "/**/*.js").permitAll().antMatchers("/api/auth/**").permitAll()
          //      .antMatchers(HttpMethod.GET, "/actuator/**", "/trace/**","/trace","/swagger","/swagger*").permitAll()
            //    .anyRequest().authenticated();
        http .csrf().disable() .authorizeRequests() .anyRequest().permitAll();
        // Add custom JWT security filter
        //http.addFilterBefore(authenticationFilter(), UsernamePasswordAuthenticationFilter.class);

    }

}