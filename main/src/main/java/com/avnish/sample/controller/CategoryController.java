package com.avnish.sample.controller;

import com.avnish.sample.model.payload.CategoryRequest;
import com.avnish.sample.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author avnish
 */
@RestController
@RequestMapping("/category")
@Api
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/list")
    @ApiOperation(value = "search Categorys", response = ResponseEntity.class)
    public ResponseEntity<?> searchCategory() {
        return ResponseEntity.ok(categoryService.getAllCategories());
    }

    @PostMapping("/add")
    @ApiOperation(value = "add new Category in the System ", response = ResponseEntity.class)
    public ResponseEntity<?> addCategory(@RequestHeader("X-User-Id") String userId,
            @Valid @RequestBody CategoryRequest categoryRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(categoryService.createCategory(userId, categoryRequest));
    }

    @PutMapping("/{categoryId}")
    @ApiOperation(value = "update existing Category", response = ResponseEntity.class)
    public ResponseEntity<?> updateCategory(@RequestHeader("X-User-Id") String userId,
            @PathVariable("categoryId") String categoryId, @Valid @RequestBody CategoryRequest categoryRequest) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(categoryService.updateCategory(userId, categoryId, categoryRequest));
    }

    @GetMapping("/{categoryId}")
    @ApiOperation(value = "Get specific Category", response = ResponseEntity.class)
    public ResponseEntity<?> getCategory(@PathVariable("categoryId") String categoryId) {
        return ResponseEntity.ok(categoryService.getCategory(categoryId));
    }

    @DeleteMapping("/{categoryId}")
    @ApiOperation(value = "Remove specific Category", response = ResponseEntity.class)
    public ResponseEntity<?> removeCategory(@PathVariable("categoryId") String categoryId) {
        categoryService.deleteCategory(categoryId);
        return ResponseEntity.ok().build();
    }
}
