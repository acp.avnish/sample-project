package com.avnish.sample.controller;

import com.avnish.sample.model.payload.SignUpRequest;
import com.avnish.sample.service.UserDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author avnish
 */
@RestController
@Api("User")
@RequestMapping("/api/auth")
public class UserController {

    @Autowired
    private UserDetailsService userDetailsService;

    @PostMapping("/register")
    @ApiOperation(value = "Register new user in the System ", response = ResponseEntity.class)
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
        return userDetailsService.registerUser(signUpRequest);
    }
}