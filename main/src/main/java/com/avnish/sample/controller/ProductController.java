package com.avnish.sample.controller;

import com.avnish.sample.model.payload.ProductRequest;
import com.avnish.sample.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author avnish
 */
@RestController
@RequestMapping("/product")
@Api
public class ProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/list")
    @ApiOperation(value = "search Products", response = ResponseEntity.class)
    public ResponseEntity<?> searchProduct() {
        return ResponseEntity.ok(productService.getAllProducts());
    }

    @PostMapping("/add")
    @ApiOperation(value = "add new Product in the System ", response = ResponseEntity.class)
    public ResponseEntity<?> addProduct(@RequestHeader("X-User-Id") String userId,
            @Valid @RequestBody ProductRequest productRequest) {
        return ResponseEntity.status(HttpStatus.CREATED).body(productService.createProduct(userId, productRequest));
    }

    @PutMapping("/{productId}")
    @ApiOperation(value = "update existing Product", response = ResponseEntity.class)
    public ResponseEntity<?> updateProduct(@RequestHeader("X-User-Id") String userId,
            @PathVariable("productId") String productId, @Valid @RequestBody ProductRequest productRequest) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(productService.updateProduct(userId, productId, productRequest));
    }

    @GetMapping("/{productId}")
    @ApiOperation(value = "Get specific Product", response = ResponseEntity.class)
    public ResponseEntity<?> getProduct(@PathVariable("productId") String productId) {
        return ResponseEntity.ok(productService.getProduct(productId));
    }

    @DeleteMapping("/{productId}")
    @ApiOperation(value = "Remove specific Product", response = ResponseEntity.class)
    public ResponseEntity<?> removeProduct(@PathVariable("productId") String productId) {
        productService.deleteProduct(productId);
        return ResponseEntity.ok().build();
    }
}
