package com.avnish.sample.model;

public enum RoleName {
    USER,
    ADMIN
}