package com.avnish.sample.model;

import com.avnish.sample.model.audit.UserDateAudit;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "categories",uniqueConstraints = { @UniqueConstraint(columnNames = { "name" })})
public class Category extends UserDateAudit {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Size(max = 40)
    private String categoryId;

    @NotBlank
    @Size(max = 40)
    private String name;

    @Size(max = 100)
    private String description;

    @ManyToOne(cascade={CascadeType.ALL})
    @JoinColumn(name="parent_category_id")
    private Category parentCategory;

    @OneToMany(mappedBy="parentCategory")
    private List<Category> childCategories;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public List<Category> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(List<Category> childCategories) {
        this.childCategories = childCategories;
    }

    public Category() {
    }

    public Category(@NotBlank @Size(max = 40) String name, @Size(max = 100) String description, Category parentCategory,
            List<Category> childCategories) {
        this.name = name;
        this.description = description;
        this.parentCategory = parentCategory;
        this.childCategories = childCategories;
    }
}
