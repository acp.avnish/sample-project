package com.avnish.sample.model.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * @author avnish
 */
public class CategoryRequest {
    @NotBlank
    @Size(min = 4, max = 40)
    private String name;

    @NotBlank
    @Size(min = 5, max = 1000)
    private String description;

    private String parentCategoryId;

    private List<CategoryRequest> childCategories;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(String parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public List<CategoryRequest> getChildCategories() {
        return childCategories;
    }

    public void setChildCategories(List<CategoryRequest> childCategories) {
        this.childCategories = childCategories;
    }
}