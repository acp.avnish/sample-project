package com.avnish.sample.security;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @author avnish
 */
public class SecurityConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private AuthTokenProvider authTokenProvider;

    public SecurityConfigurer(AuthTokenProvider authTokenProvider) {
        this.authTokenProvider = authTokenProvider;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        TokenAuthenticationFilter customFilter = new TokenAuthenticationFilter(authTokenProvider);
        http.exceptionHandling()
        .authenticationEntryPoint(new AuthenticationEntryPoint())
        .and()
        .addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
