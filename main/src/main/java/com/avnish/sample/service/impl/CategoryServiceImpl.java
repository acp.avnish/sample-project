package com.avnish.sample.service.impl;

import com.avnish.sample.exception.BadRequestException;
import com.avnish.sample.model.payload.CategoryRequest;
import com.avnish.sample.service.CategoryService;
import com.avnish.sample.model.Category;
import com.avnish.sample.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * @author avnish
 */
@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getCategory(String id) {
        return categoryRepository.findById(id).orElse(null);
    }

    @Override
    public Category createCategory(String userId, CategoryRequest categoryRequest) {
        Category category = getCategory(categoryRequest);
        category.setCreatedAt(Instant.now());
        category.setCreatedBy(userId);
        return categoryRepository.save(category);
    }

    private Category getCategory(CategoryRequest categoryRequest) {
        if(categoryRequest == null) {
            return null;
        }
        Category parentCategory = null;
        if(!StringUtils.isEmpty(categoryRequest.getParentCategoryId())) {
            parentCategory = getCategory(categoryRequest.getParentCategoryId());
            if (parentCategory == null) {
                throw new BadRequestException("Invalid Parent Category");
            }
        }
        List<Category> childCategories = null;
        if (!CollectionUtils.isEmpty(categoryRequest.getChildCategories())) {
            childCategories = new ArrayList<>();
            for(CategoryRequest childCategoryRequest: categoryRequest.getChildCategories()) {
                Category childCategory = getCategory(categoryRequest);
                if (childCategory != null) {
                    childCategories.add(childCategory);
                }
            }
        }
        return new Category(categoryRequest.getName(), categoryRequest.getDescription(),
                parentCategory, childCategories);
    }

    @Override
    public Category updateCategory(String userId, String categoryId, CategoryRequest categoryRequest) {
        Category category = getCategory(categoryId);
        category.setName(categoryRequest.getName());
        category.setDescription(categoryRequest.getDescription());
        return categoryRepository.save(category);
    }

    @Override
    public void deleteCategory(String categoryId) {
        categoryRepository.deleteById(categoryId);
    }
}