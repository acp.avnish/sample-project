package com.avnish.sample.service.impl;

import com.avnish.sample.repository.ProductRepository;
import com.avnish.sample.service.ProductService;
import com.avnish.sample.exception.BadRequestException;
import com.avnish.sample.model.Product;
import com.avnish.sample.model.payload.ProductRequest;
import com.avnish.sample.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

/**
 * @author avnish
 */
@Service
@Transactional
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @Override
    public Product getProduct(String id) {
        return productRepository.findById(id).orElse(null);
    }

    @Override
    public Product createProduct(String userId, ProductRequest productRequest) {
       Product product = new Product(productRequest.getName(), productRequest.getDescription(),
                new BigDecimal(productRequest.getPrice()));
        if(!StringUtils.isEmpty(productRequest.getCategoryId())) {
            if(categoryRepository.findById(productRequest.getCategoryId()).isPresent()) {
                product.setCategoryId(productRequest.getCategoryId());
            } else {
                throw new BadRequestException("Invalid category Id");
            }
        }
        product.setCreatedAt(Instant.now());
        product.setCreatedBy(userId);
        return productRepository.save(product);
    }

    @Override
    public Product updateProduct(String userId, String productId, ProductRequest productRequest) {
        Product product = getProduct(productId);
        if(!product.getCategoryId().equals(productRequest.getCategoryId())) {
            if(categoryRepository.findById(productRequest.getCategoryId()).isPresent()) {
                product.setCategoryId(productRequest.getCategoryId());
            } else {
                throw new BadRequestException("Invalid category Id");
            }
        }
        product.setName(productRequest.getName());
        product.setPrice(new BigDecimal(productRequest.getPrice()));
        product.setDescription(productRequest.getDescription());
        return productRepository.save(product);
    }

    @Override
    public void deleteProduct(String productId) {
        productRepository.deleteById(productId);
    }
}