package com.avnish.sample.service;

import com.avnish.sample.model.payload.SignUpRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * @author avnish
 */
public interface UserDetailsService {

    public UserDetails loadUserById(String userId);

    public ResponseEntity registerUser(SignUpRequest signUpRequest);
}