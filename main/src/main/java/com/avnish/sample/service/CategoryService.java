package com.avnish.sample.service;


import com.avnish.sample.model.Category;
import com.avnish.sample.model.payload.CategoryRequest;

import java.util.List;

/**
 * @author avnish
 */
public interface CategoryService {

    public List<Category> getAllCategories();

    public Category getCategory(String id);

    public Category updateCategory(String userId, String productId, CategoryRequest productRequest);

    public Category createCategory(String userId, CategoryRequest productRequest);

    public void deleteCategory(String productId);
}