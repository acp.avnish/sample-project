package com.avnish.sample.service;

import com.avnish.sample.model.Product;
import com.avnish.sample.model.payload.ProductRequest;

import java.util.List;

/**
 * @author avnish
 */
public interface ProductService {

    public List<Product> getAllProducts();

    public Product getProduct(String id);

    public Product updateProduct(String userId, String productId, ProductRequest productRequest);

    public Product createProduct(String userId, ProductRequest productRequest);

    public void deleteProduct(String productId);
}