package com.avnish.sample.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @author avnish
 */
class InvalidAuthenticationException extends AuthenticationException {
    /**
     *
     */
    private static final long serialVersionUID = -761503632186596342L;

    InvalidAuthenticationException(String e) {
        super(e);
    }
}