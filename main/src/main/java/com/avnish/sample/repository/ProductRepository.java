package com.avnish.sample.repository;

import com.avnish.sample.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author avnish
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
}