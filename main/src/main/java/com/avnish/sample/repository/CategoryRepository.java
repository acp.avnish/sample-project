package com.avnish.sample.repository;

import com.avnish.sample.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author avnish
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, String> {
}