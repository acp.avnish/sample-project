package com.avnish.sample.main.sample.controller;

import com.avnish.sample.main.sample.model.Product;
import com.avnish.sample.main.sample.security.AuthTokenProvider;
import com.avnish.sample.main.sample.service.ProductService;
import com.google.gson.Gson;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ProductController.class)
public class ProductControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductService productService;

    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private AuthTokenProvider authTokenProvider;
    @MockBean
    private AuthenticationEntryPoint authenticationEntryPoint;
    @MockBean
    private com.avnish.sample.main.sample.service.UserDetailsService usersDetailsService;

    private Product mockProduct = new Product("Course1", "Spring", new BigDecimal(12));
    private Gson gson = new Gson();

    @Test
    public void searchProduct() throws Exception {
        List<Product> products = new ArrayList<>();
        products.add(mockProduct);
        Mockito.when(productService.getAllProducts()).thenReturn(products);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/product/list").accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse());

        JSONAssert.assertEquals(gson.toJson(products), result.getResponse().getContentAsString(), false);
    }

    @Test
    public void getProduct() throws Exception {
        Mockito.when(productService.getProduct("1234")).thenReturn(mockProduct);

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/product/1234").accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        System.out.println(result.getResponse());

        JSONAssert.assertEquals(gson.toJson(mockProduct), result.getResponse().getContentAsString(), false);
    }

}
