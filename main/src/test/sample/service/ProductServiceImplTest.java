package com.avnish.sample.main.sample.service;

import com.avnish.sample.main.sample.model.Category;
import com.avnish.sample.main.sample.model.Product;
import com.avnish.sample.main.sample.model.payload.ProductRequest;
import com.avnish.sample.main.sample.repository.CategoryRepository;
import com.avnish.sample.main.sample.repository.ProductRepository;
import com.avnish.sample.main.sample.service.impl.ProductServiceImpl;
import com.google.gson.Gson;
import org.json.JSONException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class ProductServiceImplTest {
    @MockBean
    private ProductRepository productRepository;
    @MockBean
    private CategoryRepository categoryRepository;

    private Gson gson = new Gson();
    ProductService productService = new ProductServiceImpl();
    private ProductRequest productRequest = new ProductRequest("Name Test", "Description Test", 12d, "1234");

    @Test
    public void createProduct() throws JSONException {
        String userId = "1234";
        Product product = new Product(productRequest.getName(), productRequest.getDescription(),
                new BigDecimal(productRequest.getPrice()));
        ReflectionTestUtils.setField(productService, "productRepository", productRepository);
        ReflectionTestUtils.setField(productService, "categoryRepository", categoryRepository);
        Mockito.when(categoryRepository.findById(productRequest.getCategoryId()))
                .thenReturn(Optional.of(new Category()));
        product.setCreatedAt(Instant.now());
        product.setCreatedBy(userId);
        Mockito.when(productRepository.save(Mockito.any())).thenReturn(product);
        JSONAssert.assertEquals(gson.toJson(product), gson.toJson(productService.createProduct(userId, productRequest)),
                true);
    }


    @Test
    public void createProduct_withNoCat() throws JSONException {
        String userId = "1234";
        Product product = new Product(productRequest.getName(), productRequest.getDescription(),
                new BigDecimal(productRequest.getPrice()));
        Mockito.when(categoryRepository.findById(productRequest.getCategoryId()))
                .thenReturn(Optional.of(new Category()));
        product.setProductId("123");
        Mockito.when(productRepository.save(product)).thenReturn(product);
        product.setCreatedAt(Instant.now());
        product.setCreatedBy(userId);
        JSONAssert.assertEquals(gson.toJson(product), gson.toJson(productService.createProduct(userId, productRequest)),
                true);
    }
}
